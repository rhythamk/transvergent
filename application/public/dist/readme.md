#----------Installation Instructions--------------#

1.) Install Node.Js on Ubuntu
-------------------------------
ufw allow 3000


2.) Install Apache
-------------------------------
sudo apt install apache


3.) copy application.zip folder to /var/www/html
-------------------------------
unzip application.zip
cd application

4.) Run npm install in /var/www/html/application
-------------------------------
npm install

5.) Install MongoDB Latest Version.
-------------------------------
https://www.digitalocean.com/community/tutorials/how-to-install-mongodb-on-ubuntu-18-04

6.) Import MongoDB DB 
-------------------------------
mongorestore  --db=myapp /var/www/html/application/myapp
ufw allow 27017
sudo systemctl restart mongodb