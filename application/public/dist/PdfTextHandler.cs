﻿using Amazon.Textract.Model;
using System.Collections.Generic;
namespace CTrack.Core
{
    internal class PdfTextHandler
    {
        private readonly TextractTextDetectionService textractTextService;

        public PdfTextHandler(TextractTextDetectionService textractTextService)
        {
            this.textractTextService = textractTextService;
        }

        internal List<GetDocumentTextDetectionResponse> Handle(string bucketName, string pdfFile)
        {
            var jobId = textractTextService.StartDocumentTextDetection(bucketName, pdfFile);
            textractTextService.WaitForJobCompletion(jobId);
            return textractTextService.GetJobResults(jobId);
        }
        internal List<GetDocumentTextDetectionResponse> HandleJobFinal(string jobId)
        {
         
            return textractTextService.GetJobResults(jobId);
        }
        internal string HandleJob(string bucketName, string pdfFile)
        {
            var jobId = textractTextService.StartDocumentTextDetection(bucketName, pdfFile);
            return jobId;
        }

    }
}
