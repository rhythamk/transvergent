var express = require('express');
var router = express.Router();
var nodeMailer = require('nodemailer');
//var xoauth2 = require('xoauth2');


// declare vars
let toMail =   'prasad.ddv@gmail.com';
//let fromMail = 'hencedesklocal09@gmail.com';
let fromMail = 'understandingtree@gmail.com ';
let urlLogin= 'http://161.35.2.53:3000';
let urlFP= 'http://161.35.2.53:3000/PasswordRecovery.html?for=';
let name = 'tree';
let subject  = 'Welcome to Understanding Tree';
let text = '';


var transporter = nodeMailer.createTransport(
    {
      pool: true,
      service: 'Gmail',  
      auth: {
        type: 'OAuth2',
        user: fromMail,
        clientId: '795461869455-u8ctt06p05q7akcupnp44k0e621jpa6r.apps.googleusercontent.com',
        clientSecret: 'lIHuv8cLpWNoFdVLYHZga7vk',
        refreshToken: '1//04cOCHUfQsnX9CgYIARAAGAQSNwF-L9IryBJ99Gp5y7ULMHopUHr2P1lIqEG2ybLbcg29YSlVBEAJ2xGxDPZayHkeTxoW3qGbzDU',
        // accessToken: 'ya29.a0AfH6SMAlv3uBY6SThR2PuxpLtIm1d-T8_-U-Bi3aYFuF3Mm8u0w7rdRKUKejLUOp7gfwwH1CgNwhLQ3gD22paGa1bZZ4VOUXOjn2VdRfxxJ5joUMvwr2_xtDV28DRIaHJRFvNgFBFgYu1f4GlXFJS5uWX8KF_qWazeA',
        // expires: 1484314697598
      }
    }
  )

router.post('/send-email', function (req, res) {
 
 toMail = req.body.Email;
 name = req.body.user;
 text = '';

 text = `Hello<b> ${name}</b><br/>
 Welcome to The Understanding Tree<br/>
 The Understanding Tree can transform a groups’ culture without structural changes. It has the power to instill passion in the apathetic; it provides groups the ability to innovate and to dissolve barriers.  
 Let’s get started. Click here to <a href="${urlLogin}">login</a><br/><br/>
 
 Kind regards,<br/>
 The Understanding Tree 
 `;


    let mailOptions = {
        from: fromMail,
        to: toMail,
        subject: subject,
        text: text,
        html: text
    };
    console.log(mailOptions);
    
    transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
            return console.log(error);
        }
        console.log('Message %s sent: %s', info.messageId, info.response);
        return console.log("OK");
        });
    });



    router.post('/send-FPemail', function (req, res) {
     toMail = req.body.Email;
     name = req.body.user;
     text = '';
    var id = req.body.id;
     text = '';
     subject = "Reset Password - Understanding Tree "
    
    
        let mailOptions = {
            from: fromMail,
            to: toMail,
            subject: subject,
            text: text,
            html: `Hello <span style="text-transform: capitalize;"><b></span> ${name}</b><br/>    
            Let's get you back into your account <br/>             
            <a href="${urlFP + id}">Reset your password</a><br/><br/>Kind regards,<br/>
            The Understanding Tree 
            `
        };
        
       transporter.sendMail(mailOptions, (error, info) => {
            if (error) {
                return error;
            }
            console.log('Message %s sent: %s', info.messageId, info.response);
            return "yes";
            });
            
            res.json({"msg":"yes"});
        }) ;     

    module.exports = router;